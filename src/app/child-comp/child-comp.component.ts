import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-child-comp',
  templateUrl: './child-comp.component.html',
  styleUrls: ['./child-comp.component.scss']
})

export class ChildCompComponent {
  @Input() hero?: string;
  @Output() voteUp = new EventEmitter<boolean>();

  heroVotes = 0;

  heroVote(): void {
    this.heroVotes++;
    this.voteUp.emit(true);
  }
}
