import { Component, OnChanges, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { ChildCompComponent } from './child-comp/child-comp.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnChanges {
@ViewChild(ChildCompComponent) private childComp!: ChildCompComponent;
@ViewChildren(ChildCompComponent) private childComponents!: QueryList<ChildCompComponent>;

  ngOnChanges(changes: SimpleChanges): void {
      console.log("ngOnChanges");
  }

  ngOnInit(): void {
  }

  voteForAll(): void {
    this.childComponents.forEach((cc)=> {
      cc.heroVote();
    })
  }

  title = 'NoToMyk';

  heroList = ['First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth'];
  totalVotes = 0;

  voteFromChild(value: boolean): void { 
    this.totalVotes++;
  }
 
}
